<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nupilios@flexibledeveloper.eu> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
 */

header('Content-Type: application/json');

$apiVersion = '0.13';
$spaceName = 'Open-Lab Vorarlberg';

$openState = getOpenState();

$spaceapiArray = [
    'api' => $apiVersion,
    'space' => $spaceName,
    'logo' => 'https://open-lab.at/images/Logo/Logo.png',
    'url' => 'https://open-lab.at',
    'location' => [
        'address' => 'M² Klaus, Treietstraße 17, Klaus, Vorarlberg, Austria',
        'lon' => 9.646033516048838,
        'lat' =>  47.30509085141842,
        'timezone' =>  'Europe/Vienna',
    ],
    'contact' => [
        'email' =>  'info@open-lab.at',
        'twitter' => '@OpenLabVlbg',
    ],
    'issue_report_channels' => [
        'email',
    ],
    'state' => [
        'open' => $openState,
    ],
    'links' => [[
         "name" => "Location Hilfe",
         "url" => "https://open-lab.at/index.php/info/location",
         "description" => "Dieser Link enthält eine genauere Beschreibung um den Raum zu finden./This links to a description on how to find us. ",
    ]],
        
];

// Add a message if openlab is closed
if (false === $openState) {
    $spaceapiArray['state']['message'] = 'Offen: Mittwochs 16h-20h / Open: Wednesday 16h-20h / Guests are welcomed';
}

print json_encode($spaceapiArray);

function getOpenState(): bool
{
    $fileName = './openState';
    $fileHandle = fopen($fileName, 'r');
    $contents = fread($fileHandle, filesize($fileName));
    fclose($fileHandle);

    if (strpos($contents,'true') !== false) {
        return true;
    }

    return false;
}

function getMessageAccordingToState(bool $state)
{
    if (true === $state) {
        return 'Wir basteln wieder an unseren Projekte und freuen uns über neue Gesichter';
    }
    
    return 'Offen: Mittwochs 16h-20h';
}

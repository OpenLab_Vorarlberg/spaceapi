# Open-Lab Vorarlbergs Space API Interface
  This is the Open-Labs Vorarlbergs implementation for the [Spaceapi](https://spaceapi.io).

## Goals
- Provide an endpoint to return a valid spaceapi.json file
- Provide an endpoint to update the json file
- Minimal setup installation
- Easy to use behind a firewall and with minimal tools on the client side

## Installation
 1. Upload the files to a webspace of your choice 
 2. Create a file named `authorization` in the same directory 
 3. Create a secret you can put into the `authorization`-file

## Updating the open state
Example to update the open state
```bash
curl -X POST -d 'open=true' -d 'authorization={your authorization string here}' {url to update_open_state.php}
```
